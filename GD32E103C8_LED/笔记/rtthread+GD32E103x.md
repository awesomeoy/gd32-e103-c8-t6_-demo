### 初探RT-Thread系统在GD32E103x芯片上的使用，点亮LED灯

#### 前言

​    随着中美贸易战的加剧，很多公司越来越重视使用国产技术的重要性。使用国产技术，一方面可规避国外对技术的封锁造成产品核心技术的断供，另一方面还可以支持国内技术的迭代和进步。

​    兆易创新国内一家致力于开发先进存储器技术和IC解决方案的公司，具有丰富的产品线，基于ARM内核开发了多款MCU，而在该篇文章中我们只介绍一款MCU的使用，其是**GD32E103C8T6**。官网地址：https://www.gigadevice.com/zh-hans/

​    RT-Thread 是国内完全自主开发的开源的实时操作系统，被广泛应用于能源、车载、医疗、消费电子等多个行业，成为国人自主开发、国内最成熟稳定和装机量最大的开源 RTOS。官网地址: https://www.rt-thread.org/index.html

#### GD32E103C8T6简介

​    该芯片是一款基于Cortex-M4开发的32bit处理器，最高主频可达到120M，大大提高了程序的运行的速度，同时芯片集成了许多通信接口和外设：GPIO、ADC、PWM、USART、I2C等。硬件引脚兼容STM32F103C8T6，可方便从硬件上直接替换，无须修改硬件电路。GD32E103C8T6具有可观的内部存储空间，**Flash：64KB，RAM：20KB**。

#### 准备工作

- **硬件准备**

  市面上基于GD32系列芯片的开发板很少，此处，由于硬件接口兼容STM32F103C8T6，所以我买了一块基于ST的开发板，直接将原来的ST芯片替换成GD32芯片。

  烧写工具使用ST-Link。

- **软件准备**

  1、Keil集成开发环境

   2、GD32E10x标准固件库和keil支持包，下载地址：http://www.gd32mcu.com/cn/download/7?kw=GD32E1，文件分别对应GD32E10x Firmware Library和GD32E10x_AddOn_V1.1.0.rar。

- Keil开发准备工作

  1、Keil导入GD32E10x软件支持包，操作按照如下图序号操作，在弹出文件选择框中选择 

  ​      GigaDevice.GD32E10x_DFP.1.1.0.pack包进行安装，该包存在下载GD32E10x_AddOn_V1.1.0.rar压缩包中。![Keil导入软件包](D:\github\gd32-e103-c8-t6_-demo\GD32E103C8_LED\笔记\Keil导入软件包.png)

​       2、Keil下载RT-Thread内核，安装选择如下图所示。此处选择安装最新版本3.1.1。     ![Keil导入RTthread](D:\github\gd32-e103-c8-t6_-demo\GD32E103C8_LED\笔记\Keil导入RTthread.png)  

​        完成上述准备工作，则可进行接下来的开发了。

#### 开发工作

- **创建工程**

  依次选择，菜单栏->Project->New uVersion Project->GigaDevice->GD32E103->GD32E103C8；接着在

  Manage Run-Time Environment窗口中选择系统内核RTOS->kernel。在创建的工程目录下新建以下文件夹：

  User、Drivers、Out。

- **加载GD标准固件库**

​        将之前下载的标准固件库GD32E10x Firmware Library的解压文件放到Drivers目录下，该库中包含外部设备库，启动汇编代码、第三方库以及示例工程代码。将**GD32E10x_Firmware_Library\Template**目录下的全部c文件和h文件复制到User文件夹下(main，systick相关文件除外）。

- 完善工程

  右击Keil工程显示栏中的工程选择**Manage Project Items**，可在管理中修改工程名称，新加组，以及添加组文件。修改完成之后，显示如下图所示。![工程架构](D:\github\gd32-e103-c8-t6_-demo\GD32E103C8_LED\笔记\工程架构.png)

  在Drivers中添加**Drivers\GD32E10x_Firmware_Library\Firmware\GD32E10x_standard_peripheral\Source**下的所有C文件和**Drivers\GD32E10x_Firmware_Library\Firmware\CMSIS\GD\GD32E10x\Source\system_gd32e10x.c**文件。

  在User中添加User目录下的所有C文件。

  在Startup中添加**Drivers\GD32E10x_Firmware_Library\Firmware\CMSIS\GD\GD32E10x\Source\ARM\startup_gd32e10x.s**

  右击Keil工程显示栏中的工程选择**Optins for Target**，在**Output选项卡**中点击**Select Folder for Objects...**按钮选择将编译目标保存到Out文件夹中，并选择**Create HEX File**。在**Listing选项卡**中点击**Select Folder for Listings...**按钮选择将编译中间生成的文件保存到Out文件夹中。

  在**C/C++选项卡**中**Define栏**定义以下宏**USE_STDPERIPH_DRIVER,GD32E10X,GD32E103V_EVAL，Include Paths**中包含所有工程用到的H文件所属目录或者文件夹。![宏定义](D:\github\gd32-e103-c8-t6_-demo\GD32E103C8_LED\笔记\宏定义.png)

  在**Debug选项卡**中选择**ST-Link Debugger**。![Debug](D:\github\gd32-e103-c8-t6_-demo\GD32E103C8_LED\笔记\Debug.png)

- **定制RT-Thread**

​       因RT-Thread系统内核已经实现了针对Cortex-M3和Cortex-M4 处理器的移植，只需要简单修改代码即可使用。删除**gd32e10x_it.c**文件中**PendSV_Handler**和**SysTick_Handler**函数，因为RT-Thread重新定义了这两个中断函数。在此点亮LED灯的应用中，选择在空闲进程中的钩子函数中更新看门狗重装载计数器(喂狗操作)，所以在**rtconfig.h**将 **#define RT_USING_IDLE_HOOK**字符串前面的注释符号去掉，使得空闲钩子使能。该应用中选择开启动态分配内存，所以在**rtconfig.h**将**#define RT_USING_HEAP**去掉，开启该功能。

- **编写应用**

  该项目的应用代码全部放在了User文件夹中，详细代码工程下载地址:

  https://gitee.com/awesomeoy/gd32-e103-c8-t6_-demo

- **烧写固件运行**

​        编译完成即可通过ST-link工具将代码烧写至芯片运行。

注意：关于GD芯片固件库和软件支持下载地址：http://www.gd32mcu.com/cn/download/6?kw=GD32E1



