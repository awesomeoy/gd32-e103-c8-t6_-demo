
#ifndef HARDWARE_CONFIG_H
#define HARDWARE_CONFIG_H
	
	#include "gd32e10x.h"
	#include "systick.h"
	
	// LED相关定义
	void led_init(void);
	void led_toggle(void);
	void led_on(void);
	void led_off(void);
	
	// 复位看门狗定义
	void fwdgt_init(void);
	void fwdgt_update(void);

#endif
