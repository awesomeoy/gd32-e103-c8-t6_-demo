
#include "HardwareConfig.h"

void led_init(void)
{
	rcu_periph_clock_enable(RCU_GPIOB);
	gpio_init(GPIOB, GPIO_MODE_OUT_PP, GPIO_OSPEED_10MHZ, GPIO_PIN_12);
	
	led_off();
}

void led_toggle(void)
{
	static char on = 0;
	
	if( on )
  {
	  led_off();
		on = 0; 
	}else{
		led_on();
		on = 1;
	}
}

void led_on(void)
{
	gpio_bit_reset(GPIOB, GPIO_PIN_12);
}

void led_off(void)
{
	gpio_bit_set(GPIOB, GPIO_PIN_12);
}
