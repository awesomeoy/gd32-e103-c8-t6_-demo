
#ifndef APP_H
#define APP_H
	#include "hardwareconfig.h"
	#include <rtthread.h>
	
	void app_init(void);
	
	void app(void);
	
	void led_run(void);
	
	void thread_20ms(void* arg);
	
#endif
