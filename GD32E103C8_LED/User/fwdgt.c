
#include "hardwareconfig.h"

void fwdgt_init(void)
{
	
	fwdgt_config(2500,FWDGT_PSC_DIV8);  //看门狗 500ms=2500*8/40K
	
	fwdgt_write_disable();              // 写保护看门狗定时器寄存器
		
	fwdgt_enable();
	
	fwdgt_counter_reload();
}

void fwdgt_update(void)
{
	fwdgt_counter_reload();
}
