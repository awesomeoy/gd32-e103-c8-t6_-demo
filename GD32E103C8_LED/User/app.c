
#include "app.h"
#include "rtthread.h"

void app(void)
{
	app_init();
	
	while(1)
	{
		led_toggle();
		rt_thread_mdelay(1000);
	}
}

void app_init(void)
{
	fwdgt_init();
	led_init();
	fwdgt_update();
	
	rt_thread_idle_sethook(fwdgt_update);    //喂狗操作在空闲进程中进行
	
	/*
	rt_thread_t add = rt_thread_create("ADD",
                            thread_add,
                            NULL,
                            128,
                            RT_THREAD_PRIORITY_MAX-2,
                            10);
	if(add != RT_NULL)
        rt_thread_startup(add);
	*/
	
	fwdgt_update();
}

static void led_startup(void)
{
	uint8_t c = 0;
	
	for( c=0; c<60; ++c )
	{
		led_toggle();
		rt_thread_mdelay(50);
	}
	led_off();
}

void led_run(void)
{
	static uint16_t count = 0;
	
	++count;
	
	if( count>50 )
	{
		led_toggle();
		count = 0;
	}		
}

void thread_20ms(void* arg)
{
	led_startup();
	
	while(1)
	{
		rt_thread_mdelay(20);
		led_run();
	}
}

